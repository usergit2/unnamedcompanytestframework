package steps;

import constants.ChangeValues;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import enums.TestContextKeyEnum;
import objects.CarTypesResponse;
import objects.CarTypesResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.junit.Assert;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import utils.TestContext;
import workflows.CarTypesWorkflow;
import java.util.List;
import java.util.Map;

/**
 * Created by Rinat on 15.10.2019.
 */
public class Steps {

    private static Logger log = LoggerFactory.getLogger(Steps.class);

    @Given("^user send \"([^\"]*)\" http request with parameters to the endpoint \"([^\"]*)\"$")
    public void sendHttpReqWithParameters(String httpMethod, String endpoint, DataTable inputTable){
        List<List<String>> list = inputTable.raw();
        log.info("data table: " + list);

        MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<>();
        for(List<String> params: list){
            if(params.get(0).equals("path")){
                String key = params.get(1);
                String value = params.get(2);
                log.debug("sendDoubleHttpReqWithParameters key: " +key +"; value: " + value);
                if(value.contains(ChangeValues.CAR_TYPES_MANUFACTURER)){
                    value = TestContext.get(TestContextKeyEnum.manufacturer);
                }
                if(value.contains(ChangeValues.CAR_TYPES_MAIN_TYPE)){
                    value = TestContext.get(TestContextKeyEnum.mainType);
                }
                urlParams.add(key, value);
            }
        }
        CarTypesResponse response = CarTypesWorkflow.getCarTypesResponse(endpoint,urlParams, HttpMethod.resolve(httpMethod));
        TestContext.put(TestContextKeyEnum.responseStatus, response.getStatusCode());
        if(HttpStatus.valueOf(response.getStatusCode()).equals(HttpStatus.OK)) {
            TestContext.put(TestContextKeyEnum.reponseBody, response.getResponseBody());
        }
    }

    @Given("^user send \"([^\"]*)\" http request with parameters one after another to the endpoint \"([^\"]*)\"$")
    public void sendDoubleHttpReqWithParameters(String httpMethod, String endpoint, DataTable inputTable) {
        List<List<String>> list = inputTable.raw();
        log.info("data table: " + list);

        MultiValueMap<String, String> urlParams = new LinkedMultiValueMap<>();
        for (List<String> params : list) {
            if (params.get(0).equals("path")) {
                String key = params.get(1);
                String value = params.get(2);
                log.debug("sendDoubleHttpReqWithParameters key: " +key +"; value: " + value);
                if(value.contains(ChangeValues.CAR_TYPES_MANUFACTURER)){
                    value = TestContext.get(TestContextKeyEnum.manufacturer);
                }
                if(value.contains(ChangeValues.CAR_TYPES_MAIN_TYPE)){
                    value = TestContext.get(TestContextKeyEnum.mainType);
                }
                urlParams.add(key, value);
            }
        }
        CarTypesWorkflow.getCarTypesResponse(endpoint, urlParams, HttpMethod.resolve(httpMethod));
        CarTypesResponse response = CarTypesWorkflow.getCarTypesResponse(endpoint,urlParams,
                HttpMethod.resolve(httpMethod));
        TestContext.put(TestContextKeyEnum.responseStatus, response.getStatusCode());
        if(HttpStatus.valueOf(response.getStatusCode()).equals(HttpStatus.OK)) {
            TestContext.put(TestContextKeyEnum.reponseBody, response.getResponseBody());
        }
    }

    @Then("^response is received with status code \"([^\"]*)\"$")
    public void checkResponseStatusCode(String statusCode){
        Assert.assertNotNull("Http response code is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.responseStatus));
        int responseCode = TestContext.get(TestContextKeyEnum.responseStatus);
        log.debug("Response code: " + responseCode);
        Assert.assertTrue("Response code is different from the entered one",
                responseCode == Integer.valueOf(statusCode));
    }

    @Then("^response has \"wkda\" parameter with non null value$")
    public void checkWkdaParamHasNotNullVal(){
        Assert.assertNotNull("Http response body is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.reponseBody));
        CarTypesResponseBody responseBody = TestContext.get(TestContextKeyEnum.reponseBody);
        Assert.assertTrue("The Wkda value from the reponse is null",
                responseBody.getWkda().size() != 0);
    }

    @Then("^response has \"wkda\" parameter with null value$")
    public void checkWkdaParamHasNullVal(){
        Assert.assertNotNull("Http response body is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.reponseBody));
        CarTypesResponseBody responseBody = TestContext.get(TestContextKeyEnum.reponseBody);
        Assert.assertTrue("The Wkda value from the reponse is not null",
                responseBody.getWkda().size() == 0);
    }


    @Then("^put the first key from wkda response parameter to text context under the name: \"([^\"]*)\"$")
    public void putFirstPairToTextContext(String paramName){
        Assert.assertNotNull("Http response body is absent in Text context"
                ,TestContext.get(TestContextKeyEnum.reponseBody));
        CarTypesResponseBody response = TestContext.get(TestContextKeyEnum.reponseBody);
        Map.Entry<String,String> entry = response.getWkda().entrySet().iterator().next();
        TestContext.put(TestContextKeyEnum.valueOf(paramName), entry.getKey());
    }
}
