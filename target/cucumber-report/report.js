$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/Test.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# language: en"
    }
  ],
  "line": 4,
  "name": "Car types",
  "description": "",
  "id": "car-types",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Positive scenario",
  "description": "",
  "id": "car-types;positive-scenario",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@Positive"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 9
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 10
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 15
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 16
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 17
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 22
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 23
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 24
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: main-type}"
      ],
      "line": 25
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 589656670,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 2687144,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 71680,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 198399,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74641003,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 198826,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 72960,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 130133,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 80917265,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 538879,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 172373,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Positive double send request scenario",
  "description": "",
  "id": "car-types;positive-double-send-request-scenario",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@Positive"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "user send \"GET\" http request with parameters one after another to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 32
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 33
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 34,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "user send \"GET\" http request with parameters one after another to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 38
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 39
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 40
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "user send \"GET\" http request with parameters one after another to the endpoint \"v1/car-types/built-dates\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 45
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 46
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 47
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: main-type}"
      ],
      "line": 48
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.sendDoubleHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 153657895,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 537600,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 67414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 100267,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 80
    }
  ],
  "location": "Steps.sendDoubleHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 150398165,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 143786,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 48640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 132693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 80
    }
  ],
  "location": "Steps.sendDoubleHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 149922005,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 201386,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 56746,
  "status": "passed"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 52,
      "value": "#Due to lack of information it was considered that back end should sent succesful response with empty body"
    },
    {
      "line": 53,
      "value": "#if it is not true the test can be modified by specifying the appropriate response code and commenting out thte last step"
    }
  ],
  "line": 55,
  "name": "Invalid parameter values for manufacturer request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-manufacturer-request",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 54,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 56,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "\u003clocale\u003e"
      ],
      "line": 57
    },
    {
      "cells": [
        "path",
        "wa_key",
        "\u003cwa_key\u003e"
      ],
      "line": 58
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "response has \"wkda\" parameter with null value",
  "keyword": "Then "
});
formatter.examples({
  "line": 61,
  "name": "",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-manufacturer-request;",
  "rows": [
    {
      "cells": [
        "locale",
        "wa_key"
      ],
      "line": 62,
      "id": "car-types;invalid-parameter-values-for-manufacturer-request;;1"
    },
    {
      "cells": [
        "en",
        ""
      ],
      "line": 63,
      "id": "car-types;invalid-parameter-values-for-manufacturer-request;;2"
    },
    {
      "cells": [
        "",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 64,
      "id": "car-types;invalid-parameter-values-for-manufacturer-request;;3"
    },
    {
      "cells": [
        "",
        ""
      ],
      "line": 65,
      "id": "car-types;invalid-parameter-values-for-manufacturer-request;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 63,
  "name": "Invalid parameter values for manufacturer request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-manufacturer-request;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 54,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 56,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "matchedColumns": [
    0,
    1
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 57
    },
    {
      "cells": [
        "path",
        "wa_key",
        ""
      ],
      "line": 58
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "response has \"wkda\" parameter with null value",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74123030,
  "error_message": "cucumber.runtime.CucumberException: No response was received\r\n\tat workflows.CarTypesWorkflow.getCarTypesResponse(CarTypesWorkflow.java:33)\r\n\tat steps.Steps.sendHttpReqWithParameters(Steps.java:49)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\r\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\r\n\tat launcher.Launcher.main(Launcher.java:19)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNullVal()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 64,
  "name": "Invalid parameter values for manufacturer request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-manufacturer-request;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 54,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 56,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "matchedColumns": [
    0,
    1
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        ""
      ],
      "line": 57
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 58
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "response has \"wkda\" parameter with null value",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74249323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "duration": 178346,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNullVal()"
});
formatter.result({
  "duration": 437333,
  "error_message": "java.lang.AssertionError: The Wkda value from the reponse is not null\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat steps.Steps.checkWkdaParamHasNullVal(Steps.java:109)\r\n\tat ✽.Then response has \"wkda\" parameter with null value(features/Test.feature:60)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 65,
  "name": "Invalid parameter values for manufacturer request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-manufacturer-request;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 54,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 56,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "matchedColumns": [
    0,
    1
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        ""
      ],
      "line": 57
    },
    {
      "cells": [
        "path",
        "wa_key",
        ""
      ],
      "line": 58
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "response is received with status code \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "response has \"wkda\" parameter with null value",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72017432,
  "error_message": "cucumber.runtime.CucumberException: No response was received\r\n\tat workflows.CarTypesWorkflow.getCarTypesResponse(CarTypesWorkflow.java:33)\r\n\tat steps.Steps.sendHttpReqWithParameters(Steps.java:49)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\r\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\r\n\tat launcher.Launcher.main(Launcher.java:19)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    }
  ],
  "location": "Steps.checkResponseStatusCode(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNullVal()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 67,
      "value": "#Due to lack of information it was considered that back end should sent succesful response with empty body"
    },
    {
      "line": 68,
      "value": "#if it is not true the test can be modified by specifying the appropriate response code and commenting out thte last step"
    }
  ],
  "line": 70,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "\u003clocale\u003e"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "\u003cwa_key\u003e"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "\u003cmanufacturer\u003e"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.examples({
  "line": 80,
  "name": "",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;",
  "rows": [
    {
      "cells": [
        "locale",
        "wa_key",
        "manufacturer"
      ],
      "line": 81,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;1"
    },
    {
      "cells": [
        "en",
        "",
        "{text context: manufacturer}"
      ],
      "line": 82,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;2"
    },
    {
      "cells": [
        "",
        "coding-puzzle-client-449cc9d",
        ""
      ],
      "line": 83,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;3"
    },
    {
      "cells": [
        "",
        "",
        ""
      ],
      "line": 84,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;4"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}1"
      ],
      "line": 85,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;5"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "*"
      ],
      "line": 86,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;6"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer};"
      ],
      "line": 87,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;7"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}\u0026"
      ],
      "line": 88,
      "id": "car-types;invalid-parameter-values-for-main-types-request;;8"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 82,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        ""
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 75661162,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 68693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 75093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73562390,
  "error_message": "cucumber.runtime.CucumberException: No response was received\r\n\tat workflows.CarTypesWorkflow.getCarTypesResponse(CarTypesWorkflow.java:33)\r\n\tat steps.Steps.sendHttpReqWithParameters(Steps.java:49)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\r\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\r\n\tat launcher.Launcher.main(Launcher.java:19)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 83,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        ""
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        ""
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 78356413,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 75519,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 121173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 77523134,
  "status": "passed"
});
formatter.scenario({
  "line": 84,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        ""
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        ""
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        ""
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 81525690,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 76373,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 108800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72432579,
  "error_message": "cucumber.runtime.CucumberException: No response was received\r\n\tat workflows.CarTypesWorkflow.getCarTypesResponse(CarTypesWorkflow.java:33)\r\n\tat steps.Steps.sendHttpReqWithParameters(Steps.java:49)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\r\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\r\n\tat launcher.Launcher.main(Launcher.java:19)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 85,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}1"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76622441,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 80640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 95147,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76229482,
  "status": "passed"
});
formatter.scenario({
  "line": 86,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "*"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74891030,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 52479,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 78933,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72839618,
  "status": "passed"
});
formatter.scenario({
  "line": 87,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer};"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76732521,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 61866,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 108800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 71856579,
  "status": "passed"
});
formatter.scenario({
  "line": 88,
  "name": "Invalid parameter values for main types request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-main-types-request;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 69,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 72
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 73
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 77
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 78
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}\u0026"
      ],
      "line": 79
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 77137854,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 107519,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 84053,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76388202,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 91,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "\u003clocale\u003e"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "\u003cwa_key\u003e"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "\u003cmanufacturer\u003e"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "\u003cmain-type\u003e"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.examples({
  "line": 108,
  "name": "",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;",
  "rows": [
    {
      "cells": [
        "locale",
        "wa_key",
        "manufacturer",
        "main-type"
      ],
      "line": 109,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;1"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}",
        ""
      ],
      "line": 110,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;2"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "",
        "{text context: main-type}"
      ],
      "line": 111,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;3"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "",
        ""
      ],
      "line": 112,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;4"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}",
        "{text context: manufacturer}"
      ],
      "line": 113,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;5"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}",
        "*"
      ],
      "line": 114,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;6"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}",
        "{text context: main-type};"
      ],
      "line": 115,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;7"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}",
        "{text context: main-type}\u0026"
      ],
      "line": 116,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;8"
    },
    {
      "cells": [
        "en",
        "coding-puzzle-client-449cc9d",
        "{text context: manufacturer}; *",
        "\u0026 {text context: main-type}"
      ],
      "line": 117,
      "id": "car-types;invalid-parameter-values-for-built-dates-request;;9"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 110,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        ""
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 90842376,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 142506,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 253439,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 81749691,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 48640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 79787,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74086337,
  "status": "passed"
});
formatter.scenario({
  "line": 111,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        ""
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: main-type}"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76785427,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 1061119,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 281173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76996628,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 62293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 99413,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73135298,
  "status": "passed"
});
formatter.scenario({
  "line": 112,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        ""
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        ""
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72152685,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 45227,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 372480,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 75716202,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 107093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 284586,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 78226280,
  "status": "passed"
});
formatter.scenario({
  "line": 113,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: manufacturer}"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73280365,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 62293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 81493,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 75963242,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 48640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 62293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 71503726,
  "status": "passed"
});
formatter.scenario({
  "line": 114,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "*"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74424683,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 46080,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 162560,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72610925,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 51200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 56320,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74797163,
  "status": "passed"
});
formatter.scenario({
  "line": 115,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: main-type};"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 75798122,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 45654,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 59306,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76657001,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 74667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 86186,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 76862228,
  "status": "passed"
});
formatter.scenario({
  "line": 116,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "{text context: main-type}\u0026"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73739884,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 61440,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 93440,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73028205,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 55466,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 96426,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 72811031,
  "status": "passed"
});
formatter.scenario({
  "line": 117,
  "name": "Invalid parameter values for built dates request",
  "description": "",
  "id": "car-types;invalid-parameter-values-for-built-dates-request;;9",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@Regress"
    },
    {
      "line": 90,
      "name": "@Negative"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/manufacturer\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 93
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 94
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "put the first key from wkda response parameter to text context under the name: \"manufacturer\"",
  "keyword": "Then "
});
formatter.step({
  "line": 97,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/main-types\"",
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 98
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 99
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}"
      ],
      "line": 100
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "response has \"wkda\" parameter with non null value",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "put the first key from wkda response parameter to text context under the name: \"mainType\"",
  "keyword": "Then "
});
formatter.step({
  "line": 103,
  "name": "user send \"GET\" http request with parameters to the endpoint \"v1/car-types/built-dates\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "rows": [
    {
      "cells": [
        "path",
        "locale",
        "en"
      ],
      "line": 104
    },
    {
      "cells": [
        "path",
        "wa_key",
        "coding-puzzle-client-449cc9d"
      ],
      "line": 105
    },
    {
      "cells": [
        "path",
        "manufacturer",
        "{text context: manufacturer}; *"
      ],
      "line": 106
    },
    {
      "cells": [
        "path",
        "main-type",
        "\u0026 {text context: main-type}"
      ],
      "line": 107
    }
  ],
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/manufacturer",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 74946923,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 58026,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "manufacturer",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 126293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/main-types",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 73479191,
  "status": "passed"
});
formatter.match({
  "location": "Steps.checkWkdaParamHasNotNullVal()"
});
formatter.result({
  "duration": 52480,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mainType",
      "offset": 80
    }
  ],
  "location": "Steps.putFirstPairToTextContext(String)"
});
formatter.result({
  "duration": 86187,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "GET",
      "offset": 11
    },
    {
      "val": "v1/car-types/built-dates",
      "offset": 62
    }
  ],
  "location": "Steps.sendHttpReqWithParameters(String,String,DataTable)"
});
formatter.result({
  "duration": 75636416,
  "status": "passed"
});
});