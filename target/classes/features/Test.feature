# language: en

@Regress
Feature: Car types

  @Positive
  Scenario: Positive scenario
   Given user send "GET" http request with parameters to the endpoint "v1/car-types/manufacturer"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
   Then response is received with status code "200"
   And response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "manufacturer"
   When user send "GET" http request with parameters to the endpoint "v1/car-types/main-types"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
     | path   | manufacturer  | {text context: manufacturer} |
   Then response is received with status code "200"
   And response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "mainType"
   When user send "GET" http request with parameters to the endpoint "v1/car-types/built-dates"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
     | path   | manufacturer  | {text context: manufacturer} |
     | path   | main-type     | {text context: main-type}     |
   Then response is received with status code "200"
   Then response has "wkda" parameter with non null value

  @Positive
  Scenario: Positive double send request scenario
   Given user send "GET" http request with parameters one after another to the endpoint "v1/car-types/manufacturer"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
   Then response is received with status code "200"
   And response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "manufacturer"
   When user send "GET" http request with parameters one after another to the endpoint "v1/car-types/main-types"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
     | path   | manufacturer  | {text context: manufacturer} |
   Then response is received with status code "200"
   And response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "mainType"
   When user send "GET" http request with parameters one after another to the endpoint "v1/car-types/built-dates"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
     | path   | manufacturer  | {text context: manufacturer} |
     | path   | main-type     | {text context: main-type}     |
   Then response is received with status code "200"
   Then response has "wkda" parameter with non null value

#Due to lack of information it was considered that back end should sent succesful response with empty body
#if it is not true the test can be modified by specifying the appropriate response code and commenting out thte last step
  @Negative
  Scenario Outline: Invalid parameter values for manufacturer request
   Given user send "GET" http request with parameters to the endpoint "v1/car-types/manufacturer"
     | path   | locale        | <locale> |
     | path   | wa_key        | <wa_key> |
   Then response is received with status code "200"
   Then response has "wkda" parameter with null value
  Examples:
    | locale | wa_key                       |
    | en     |                              |
    |        | coding-puzzle-client-449cc9d |
    |        |                              |

#Due to lack of information it was considered that back end should sent succesful response with empty body
#if it is not true the test can be modified by specifying the appropriate response code and commenting out thte last step
  @Negative
  Scenario Outline: Invalid parameter values for main types request
   Given user send "GET" http request with parameters to the endpoint "v1/car-types/manufacturer"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
   Then response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "manufacturer"
   When user send "GET" http request with parameters to the endpoint "v1/car-types/main-types"
     | path   | locale        | <locale>       |
     | path   | wa_key        | <wa_key>       |
     | path   | manufacturer  | <manufacturer> |
  Examples:
    | locale | wa_key                       | manufacturer                  |
    | en     |                              | {text context: manufacturer}  |
    |        | coding-puzzle-client-449cc9d |                               |
    |        |                              |                               |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}1 |
    | en     | coding-puzzle-client-449cc9d | *                             |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}; |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}& |

  @Negative
  Scenario Outline: Invalid parameter values for built dates request
   Given user send "GET" http request with parameters to the endpoint "v1/car-types/manufacturer"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
   Then response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "manufacturer"
   When user send "GET" http request with parameters to the endpoint "v1/car-types/main-types"
     | path   | locale        | en                           |
     | path   | wa_key        | coding-puzzle-client-449cc9d |
     | path   | manufacturer  | {text context: manufacturer} |
   Then response has "wkda" parameter with non null value
   Then put the first key from wkda response parameter to text context under the name: "mainType"
   When user send "GET" http request with parameters to the endpoint "v1/car-types/built-dates"
     | path   | locale        | <locale>       |
     | path   | wa_key        | <wa_key>       |
     | path   | manufacturer  | <manufacturer> |
     | path   | main-type     | <main-type>    |
  Examples:
    | locale | wa_key                       | manufacturer                    | main-type                     |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}    |                               |
    | en     | coding-puzzle-client-449cc9d |                                 | {text context: main-type}     |
    | en     | coding-puzzle-client-449cc9d |                                 |                               |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}    | {text context: manufacturer}  |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}    | *                             |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}    | {text context: main-type};    |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}    | {text context: main-type}&    |
    | en     | coding-puzzle-client-449cc9d | {text context: manufacturer}; * | & {text context: main-type}   |