package workflows;

import client.HttpSpringClient;
import cucumber.runtime.CucumberException;
import objects.CarTypesResponse;
import objects.CarTypesResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import utils.Utils;

/**
 * Created by Rinat on 15.10.2019.
 */
public class CarTypesWorkflow {

    private static Logger log = LoggerFactory.getLogger(CarTypesWorkflow.class);

    public static CarTypesResponse getCarTypesResponse(String url, MultiValueMap<String, String> urlParams,
                                                           HttpMethod httpMethod){
        CarTypesResponse carTypesResponse = new CarTypesResponse();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity entity = new HttpEntity(headers);
        log.debug("HTTP request: " + entity.getHeaders());
        ResponseEntity<String> response = HttpSpringClient
                .sendHttpRequest(
                        HttpSpringClient.getUri(url,urlParams),
                        httpMethod,
                        entity,
                        String.class);
        if(response == null){
            throw new CucumberException("No response was received");
        }
        carTypesResponse.setStatusCode(response.getStatusCodeValue());
        if(response.getStatusCode()== HttpStatus.OK) {
            carTypesResponse.setResponseBody(Utils.convertJSONtoObject(response.getBody(), CarTypesResponseBody.class));
        }
        return carTypesResponse;
    }
}
