package constants;

/**
 * Created by Rinat on 18.10.2019.
 */
public final class ChangeValues {
    public static final String CAR_TYPES_MANUFACTURER = "{text context: manufacturer}";
    public static final String CAR_TYPES_MAIN_TYPE = "{text context: main-type}";
}
