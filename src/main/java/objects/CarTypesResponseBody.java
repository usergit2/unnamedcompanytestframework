package objects;

import org.springframework.util.MultiValueMap;

import java.util.Map;

/**
 * Created by Rinat on 15.10.2019.
 */
public class CarTypesResponseBody {

    private int page, pageSize,totalPageCount;
    private Map<String,String> wkda;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public Map<String, String> getWkda() {
        return wkda;
    }

    public void setWkda(Map<String, String> wkda) {
        this.wkda = wkda;
    }

    @Override
    public String toString() {
        return "CarTypesResponseBody{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                ", totalPageCount=" + totalPageCount +
                ", wkda=" + wkda +
                '}';
    }
}
