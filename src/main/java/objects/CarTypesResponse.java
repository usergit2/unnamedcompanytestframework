package objects;

/**
 * Created by Rinat on 18.10.2019.
 */
public class CarTypesResponse {

    private int statusCode;
    private CarTypesResponseBody responseBody;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public CarTypesResponseBody getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(CarTypesResponseBody responseBody) {
        this.responseBody = responseBody;
    }

    @Override
    public String toString() {
        return "CarTypesResponse{" +
                "statusCode=" + statusCode +
                ", responseBody=" + responseBody +
                '}';
    }
}
