package utils;

import enums.TestContextKeyEnum;

import java.util.HashMap;

public class TestContext {

    private static HashMap<TestContextKeyEnum,Object> context = new HashMap<>();

    public static  <T> T get(TestContextKeyEnum key) {
        return (T) context.get(key);
    }

    public static void put(TestContextKeyEnum key, Object value) {
        context.put(key, value);
    }

    @Override
    public String toString() {
        return "TestContext" +  context.toString();
    }
}
