package client;

import configuration.MainConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class HttpSpringClient {

    private static Logger log = LoggerFactory.getLogger(HttpSpringClient.class);

    public static  <T> ResponseEntity<T> sendHttpRequest(String url,
                                                         HttpMethod httpMethod,
                                                         HttpEntity entity,
                                                         Class<T> t) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<T> response = null;
        try {
            response = restTemplate.exchange(url, httpMethod, entity, t);
            log.debug("HTTP response = " + response.getBody());
        } catch (HttpClientErrorException e){
            log.error("Request was not sent or didn't receive a response.");
        }
        return response;
    }

    public static String getUri(String url, MultiValueMap<String,String> urlParams){
        String siteAdress = "";
        if(MainConfiguration.getPort() != null && MainConfiguration.getSiteAdress() != null){
            siteAdress = "http://"+ MainConfiguration.getSiteAdress() +":" + MainConfiguration.getPort() + "/" + url;
        }
        if(MainConfiguration.getPort() == null && MainConfiguration.getSiteAdress() != null){
            siteAdress = "http://"+ MainConfiguration.getSiteAdress() + "/" + url;
        }
        log.debug("urlParams: " + urlParams);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(siteAdress)
                .queryParams(urlParams);
        String uri = builder.build().toUriString();
        log.info("Generated URL: " + uri);
        return uri;
    }
}
